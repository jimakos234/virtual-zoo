INSERT INTO public.animal (id, name, species) VALUES ('68c2751c-908b-4477-bcfb-a0a2bd19dba8', 'George', 'DOG');
INSERT INTO public.animal (id, name, species) VALUES ('d5024909-9678-454b-8598-d7709cc955d2', 'John', 'DOG');
INSERT INTO public.animal (id, name, species) VALUES ('188c320c-9942-41a3-9cc7-07d5f3b478dc', 'David', 'DOG');
INSERT INTO public.animal (id, name, species) VALUES ('30427346-3e0f-4f7c-a31b-65273cf4cb38', 'Lassie', 'DOG');
INSERT INTO public.animal (id, name, species) VALUES ('eb78f56a-8f83-41e8-9933-663be1302a6b', 'Irodis', 'CAT');
INSERT INTO public.animal (id, name, species) VALUES ('dc93d250-a82b-4b55-9e75-3af463c7b21c', 'Myrto', 'CAT');
INSERT INTO public.animal (id, name, species) VALUES ('cff2a520-7ee0-4957-b821-d68c38f8c357', 'Nemo', 'FISH');

INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('68c2751c-908b-4477-bcfb-a0a2bd19dba8', 'SIT');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('68c2751c-908b-4477-bcfb-a0a2bd19dba8', 'JUMP');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('d5024909-9678-454b-8598-d7709cc955d2', 'BARK');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('d5024909-9678-454b-8598-d7709cc955d2', 'JUMP');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('188c320c-9942-41a3-9cc7-07d5f3b478dc', 'ROLL_OVER');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('188c320c-9942-41a3-9cc7-07d5f3b478dc', 'BARK');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('188c320c-9942-41a3-9cc7-07d5f3b478dc', 'JUMP');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('30427346-3e0f-4f7c-a31b-65273cf4cb38', 'ROLL_OVER');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('30427346-3e0f-4f7c-a31b-65273cf4cb38', 'SIT');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('30427346-3e0f-4f7c-a31b-65273cf4cb38', 'BARK');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('eb78f56a-8f83-41e8-9933-663be1302a6b', 'SIT');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('eb78f56a-8f83-41e8-9933-663be1302a6b', 'MEOW');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('dc93d250-a82b-4b55-9e75-3af463c7b21c', 'SIT');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('dc93d250-a82b-4b55-9e75-3af463c7b21c', 'JUMP');
INSERT INTO public.animal_tricks (animal_id, tricks) VALUES ('dc93d250-a82b-4b55-9e75-3af463c7b21c', 'MEOW');