package gr.inaccess.virtualzoo.repository;

import gr.inaccess.virtualzoo.enums.Species;
import gr.inaccess.virtualzoo.model.Animal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ZooRepository extends JpaRepository<Animal, UUID> {

	Optional<Animal> findAnimalById(UUID uuid);

	List<Animal> findAnimalBySpecies(Species species);

}
