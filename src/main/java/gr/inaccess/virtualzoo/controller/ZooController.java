package gr.inaccess.virtualzoo.controller;

import gr.inaccess.virtualzoo.enums.Species;
import gr.inaccess.virtualzoo.enums.Tricks;
import gr.inaccess.virtualzoo.model.Animal;
import gr.inaccess.virtualzoo.service.ZooService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/animals")
public class ZooController {

   private static final Logger logger = LoggerFactory.getLogger(ZooController.class);

   private final ZooService zooService;

   @Autowired
   public ZooController(ZooService zooService) {
      this.zooService = zooService;
   }

   @ApiOperation("Fetch All animals of the zoo")
   @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<List<Animal>> getAnimals(){
      try {
         return ResponseEntity.ok(zooService.fetchAllAnimals());
      }catch (Exception e){
         logger.error("An error occurred on getAnimals: ", e);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
      }
   }

   @ApiOperation("Fetch All animals specified by species")
   @GetMapping(value = "/all/{species}", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<List<Animal>> getAnimalsByType(@PathVariable String species){
      try{
         Species speciesType = Species.fromString(StringUtils.toRootUpperCase(species));
         logger.info("Species found: {}", speciesType);
         if (speciesType == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
         }
         List<Animal> animalList = zooService.fetchAllAnimalBySpecies(speciesType);
         return ResponseEntity.ok(animalList);
      }catch (Exception e) {
         logger.error("An error occurred on getAnimalsByType: ", e);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
      }
   }

   @ApiOperation("Makes the animal with the given Id do a trick")
   @GetMapping(value = "/{animalId}/doTrick", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<Map<String, Tricks>> doTrick(@PathVariable String animalId){
      try{
         logger.info("Animal uuid: {}", animalId);
         return ResponseEntity.ok(Collections.singletonMap("trick", zooService.performTrickByAnimalId(animalId)));
      }catch (Exception e) {
         logger.error("An error occurred on doTrick: ", e);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
      }
   }

   @ApiOperation("Learns to the animal with the given Id a new trick")
   @GetMapping(value = "/{animalId}/learnTrick", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<Map<String,String>> learnTrick(@PathVariable String animalId){
      try{
         logger.info("Animal uuid: {}", animalId);
         return ResponseEntity.ok(Collections.singletonMap("Response",zooService.learnTrickToSpecifiedAnimal(animalId)));
      }catch (Exception e) {
         logger.error("An error occurred on learnTrick: ", e);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
      }
   }
}
