package gr.inaccess.virtualzoo.model;

import gr.inaccess.virtualzoo.enums.Species;
import gr.inaccess.virtualzoo.enums.Tricks;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Animal {

   @ElementCollection(targetClass=Tricks.class,fetch = FetchType.EAGER)
   @Enumerated(EnumType.STRING) // Possibly optional (I'm not sure) but defaults to ORDINAL.
   @CollectionTable(name="animal_tricks")
   @Column(name="tricks")
   private Set<Tricks> tricks;

   @Column(name = "name")
   private String name;
   @Id
   @GeneratedValue
   private UUID id;
   @Column(name = "species")
   @Enumerated(EnumType.STRING)
   private Species species;

   private void generateId(){
      this.id= UUID.randomUUID();
   }

   public UUID getId(){
      return this.id;
   }

   public Animal(String name, Species species, Set<Tricks> tricks) {
      generateId();
      this.name = name;
      this.species = species;
      this.tricks = tricks;
   }

   public Animal() {
      generateId();
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Species getSpecies() {
      return species;
   }

   public void setSpecies(Species species) {
      this.species = species;
   }

   public Set<Tricks> getTricks() {
      if(null == tricks){
         return new HashSet<>();
      }
      return tricks;
   }

   public void setTricks(Set<Tricks> tricks) {
      this.tricks = tricks;
   }

   @Override
   public String toString() {
      return "Animal{" +
         "tricks=" + tricks +
         ", name='" + name + '\'' +
         ", id=" + id +
         ", species=" + species +
         '}';
   }
}
