package gr.inaccess.virtualzoo.service;

import gr.inaccess.virtualzoo.enums.Species;
import gr.inaccess.virtualzoo.enums.Tricks;
import gr.inaccess.virtualzoo.model.Animal;

import java.util.List;

public interface ZooService {

   /**
    * Fetch all animals available in the database.
    * @return List of {@link Animal}
    */
   List<Animal> fetchAllAnimals();

   /**
    * Fetch all animals matching the given species available in the database.
    * @param  species species
    * @return List of {@link Animal}
    */
   List<Animal> fetchAllAnimalBySpecies(Species species);

   /**
    * Makes the animal with the given Id, do a trick.
    * @param uuid animal uuid
    * @return {@link Tricks}
    */
   Tricks performTrickByAnimalId(String uuid);

   /**
    * Learn a trick to the specified animal.
    * @param uuid animal uuid
    * @return info about the result
    */
   String learnTrickToSpecifiedAnimal(String uuid);

}
