package gr.inaccess.virtualzoo.service;

import static gr.inaccess.virtualzoo.enums.Tricks.BARK;
import static gr.inaccess.virtualzoo.enums.Tricks.JUMP;
import static gr.inaccess.virtualzoo.enums.Tricks.MEOW;
import static gr.inaccess.virtualzoo.enums.Tricks.ROLL_OVER;
import static gr.inaccess.virtualzoo.enums.Tricks.SIT;

import gr.inaccess.virtualzoo.enums.Species;
import gr.inaccess.virtualzoo.enums.Tricks;
import gr.inaccess.virtualzoo.model.Animal;
import gr.inaccess.virtualzoo.repository.ZooRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class ZooServiceImpl implements ZooService {

   private static final Logger logger = LoggerFactory.getLogger(ZooServiceImpl.class);
   private final Set<Tricks> dogTricks = Set.of(JUMP, SIT, ROLL_OVER, BARK);
   private final Set<Tricks> catTricks = Set.of(JUMP, SIT, MEOW);

   private final ZooRepository zooRepository;

   @Autowired
   public ZooServiceImpl(ZooRepository zooRepository) {
      this.zooRepository = zooRepository;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public List<Animal> fetchAllAnimals() {
      return zooRepository.findAll();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public List<Animal> fetchAllAnimalBySpecies(Species species) {
      return zooRepository.findAnimalBySpecies(species);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Tricks performTrickByAnimalId(String uuid) {
      Optional<Animal> optional = zooRepository.findAnimalById(UUID.fromString(uuid));
      if(optional.isPresent()){
         Animal animal = optional.get();
         logger.info("Animal found: {}",animal);
         Set<Tricks> tricksSet = animal.getTricks();
         /*
          * perform a random trick
          */
         return  tricksSet.stream()
            .skip((int) (tricksSet.size() * Math.random())).findAny().orElseThrow();
      }
      throw new NoSuchElementException();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String learnTrickToSpecifiedAnimal(String uuid) {
      Optional<Animal> optional = zooRepository.findAnimalById(UUID.fromString(uuid));
      if(optional.isPresent()){
         Animal animal = optional.get();
         logger.info("Animal found: {}",animal);
         String result = null;
         switch (animal.getSpecies()){
            case DOG:
               result = addRandomAnimalTrick(animal, dogTricks);
               break;
            case CAT:
               result = addRandomAnimalTrick(animal, catTricks);
               break;
            //case FISH:
            default:
               result = "Fish cant learn any tricks";
               logger.info("Fish cant learn any tricks");
               break;
         }
         return result;
      }
      throw new NoSuchElementException();
   }

   /**
    * Learns a random unknown trick to an animal, if any.
    * @param animal animal
    * @param animalTricks animalTricks
    * @return result
    */
   private String addRandomAnimalTrick(Animal animal, Set<Tricks> animalTricks) {
      Tricks tricks;
      if (animal.getTricks().size() == animalTricks.size()){
         return "Nothing new to learn";
      }
      /*
       * find a random applicable trick every time
       * if the animal already knows it, retry with another random trick
       */
      do {
         tricks = animalTricks.stream()
            .skip((int) (animalTricks.size() * Math.random())).findAny().orElseThrow();
      } while (animal.getTricks().contains(tricks));
      animal.getTricks().add(tricks);
      zooRepository.saveAndFlush(animal);
      return "Learned to "+ tricks;
   }
}
