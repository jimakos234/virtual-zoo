package gr.inaccess.virtualzoo.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Species {

   CAT("CAT"),
   DOG("DOG"),
   FISH("FISH");

   private final String species;

   Species(String species) {
      this.species = species;
   }

   private static final Map<String, Species> reverseMap = new HashMap<>();
   static {
      Arrays.stream(values()).forEach(enumeration -> reverseMap.put(enumeration.getSpecies(), enumeration));
   }

   @JsonValue
   public String getSpecies() {
      return species;
   }

   public static Species fromString(String s) {
      return reverseMap.get(s);
   }

   @Override
   public String toString() {
      return species;
   }
}
