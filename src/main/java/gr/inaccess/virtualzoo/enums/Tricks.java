package gr.inaccess.virtualzoo.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Tricks {

   JUMP("Jump"),
   SIT("Sit"),
   ROLL_OVER("Roll Over"),
   MEOW("Meow"),
   BARK("Bark");

   private final String trick;

   Tricks(String trick) {
      this.trick = trick;
   }

   private static final Map<String, Tricks> reverseMap = new HashMap<>();
   static {
      Arrays.stream(values()).forEach(enumeration -> reverseMap.put(enumeration.getTrick(), enumeration));
   }

   @JsonValue
   public String getTrick() {
      return trick;
   }

   public static Tricks fromString(String s) {
      return reverseMap.get(s);
   }

   @Override
   public String toString() {
      return trick;
   }
}
