# Virtual Zoo

Virtual Zoo Inaccess Test

## Requirements

In order to run the application, you are going to need the following installed:

- OpenJDK 11  ([https://jdk.java.net/java-se-ri/11](https://jdk.java.net/java-se-ri/11))
- Maven 3.8.1+
- Docker Desktop



## Clone the files

```
cd your_repo
git clone git@gitlab.com:jimakos234/virtual-zoo.git
```

or via https

```
cd your_repo
git clone https://gitlab.com/jimakos234/virtual-zoo.git
```

## Build the project

Using Maven 3.8.1 with jdk11
```
cd your_repo/virtual-zoo
mvn clean install
```

If you have a different java version as JAVA_HOME, install jdk11 to

``
C:\Program Files\Java\jdk-11
``

and run using the following from cmd:

```
mvnw.cmd clean install
```

## Run using Docker

In order to run the project after we have completed the build process, 
we use the following command on the same folder

```
cd your_repo/virtual-zoo
docker-compose -f docker-compose.yml up -d
```

In order to close the app, use the following command
```
cd your_repo/virtual-zoo
docker-compose -f docker-compose.yml down
```

## Accessing the app

The swagger can be found on the following address:
```
http://localhost:8080/myZoo/swagger-ui/
```

All the available endpoints that are in the exercise(plus the extra) will be shown
in the swagger-ui

Some data have been added in the DB, for testing purposes.
