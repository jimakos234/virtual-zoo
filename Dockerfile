FROM openjdk:11.0-jdk-slim
VOLUME /tmp
ADD target/virtualzoo*.jar virtualzoo.jar
ENV JAVA_OPTS="-Xms128m -Xmx512m "
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /virtualzoo.jar" ]